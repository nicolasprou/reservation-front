declare type EquipementType = {
  name: string;
};

declare type OptionSalleType = {
  name: string;
  description: string;
  capacity: number;
  equipements: EquipementType[];
  value: string;
};

declare type DateType = {
  jour: number;
  mois: number;
  annee: number;
  heureDebut: undefined | number;
  heureFin: undefined | number;
};

declare type ReservationType = {
  nomSalle: string;
  dateReservation: {
    jour: number;
    mois: number;
    annee: number;
    heureDebut: number;
    heureFin: number;
  };
};
function isArraysEqualsIgnoreOrder(a: EquipementType[], b: EquipementType[]) {
  if (a.length !== b.length) return false;
  const uniqueValues = Array.from(new Set([...a, ...b]));
  for (const v of uniqueValues) {
    const aCount = a.filter((e) => e.name === v.name).length;
    const bCount = b.filter((e) => e.name === v.name).length;
    if (aCount !== bCount) return false;
  }
  return true;
}

export function contientFiltreActif(
  equipements: EquipementType[],
  listeFiltres: EquipementType[]
) {
  if (listeFiltres.length === 0) {
    return true;
  }
  if (listeFiltres.length === 1) {
    return listeFiltres.some((f) => equipements.find((e) => e.name === f.name));
  }
  return isArraysEqualsIgnoreOrder(equipements, listeFiltres);
}

export function isSalleDispo(
  s: OptionSalleType,
  date: DateType,
  reservations: ReservationType[]
): boolean {
  const listeReservations = reservations.filter(
    (r) =>
      r.dateReservation.annee === date.annee &&
      r.dateReservation.mois === date.mois &&
      r.dateReservation.jour === date.jour &&
      r.dateReservation.heureDebut === date.heureDebut &&
      r.dateReservation.heureFin === date.heureFin
  );
  return !listeReservations.some((r) => r.nomSalle === s.value);
}
