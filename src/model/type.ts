import { IconDefinition } from "@fortawesome/fontawesome-common-types";

export declare type EquipementType = {
  name: string;
};

export declare type ReservationType = {
  id: string;
  nomSalle: string;
  dateReservation: {
    jour: number;
    mois: number;
    annee: number;
    heureDebut: number;
    heureFin: number;
  };
};

export declare type OptionSalleType = {
  name: string;
  description: string;
  capacity: number;
  equipements: EquipementType[];
  value: string;
};

export declare type DateType = {
  jour: number;
  mois: number;
  annee: number;
  heureDebut: undefined | number;
  heureFin: undefined | number;
};
